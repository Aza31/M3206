Avec la commande : find . -name *.sh -exec ls {} \; On peut lister tous les fichier .sh du repertoitre courant
Avec la commande find tdfind/ -name *.txt -ok rm {} \;on va supprimer les fichiers txt du repertoire /tdfindmais avant de le faire on va demander la permission a l'utilisateur qui repondra par 'y' ou 'n'
Avec la commande find .-name *.txt | cpio -ov > txt_files.cpio on va chercher les fichiers txt avec find, puis on va les mettre dans une archive .cpio que j'appelle ici txt_files.cpioPuis si on veut extraire cette archive on peut faire : cpio -idv < txt_files.cpio
La commande find . -name "*.txt" -exec head -1 {} \; permet de lire la premiere ligne de toutles fichiers texte du repertoire actuel 
