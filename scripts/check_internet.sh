#!/bin/sh
echo "[...] Checking internet connection [...]" #Informe ce que l'on teste
if ping -q -c 5 www.jeuxvideo.com > /dev/null   #ping de maniere silencieuse et met le resultat dans /dev/null
    then    #Si ça marche
	echo "[...] Internet Access OK [...]"
	exit 0
    else    #Sinon
	echo "[/!\] Not connected to Internet  [/!\]"
	echo "[/!\] Please check configuration [/!\]"
	exit 1
fi
