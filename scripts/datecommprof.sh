#!/bin/bash
f="first"
l="last"

if [ $1 = $f ] #Si first on garde la premiere occurence avec head -1
then
time=$(cat $3 | awk '{print $2}' | grep $2| cut -d ':' -f1 | head -1 )
fi


if [ $1 = $l ] #Sinon on utilise tail pour garder la derniere occurence
then
time=$(cat $3 | awk '{print $2}' | grep $2| cut -d ':' -f1 | tail -1 )
fi

if [ $1 != $f ] && [ $1 != $l ] #Si le premier parametre est incorrect
then
echo " First parameter is 'last' of 'first' "
echo " usage : ./datecommprof.sh [last|first] [command] [file]"
fi

if [ ! -e $3 ] #Si le fichier en parametre est incorrect
then
echo " Le fichier "$3" n'existe pas " 
echo " usage : ./datecommprof.sh [last|first] [command] [file]"
fi

nbcom=$( cat /root/my_history | awk '{print$2}' | grep $2 | wc -l) #Variable pour verifier que la commande passer en parametre existe
if [ $nbcom -eq 0 ]
then
echo " Cette commande n'apparait pas dans my_history "
fi

date -d @$time
