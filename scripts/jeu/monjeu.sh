#!/bin/bash

if [ "$0" == "./monjeu.sh" ]; then
    echo "Le script est nommé et lancé correctement"
    echo "Creer maintenant un fichier test"
	if [ -e "test" ];then
	echo "Le fichier test existe passons a la suite"
   	echo "Changer les droit du fichier test en rw-r-----"
	
	droit=$(stat -c %a test|shasum | awk '{print $1}')  #Commande pour recuperer les droits en octal
	if [ $droit == b8b52380bbb19b858f5b74122304db9d2fe5f61f  ]; then
	echo "Les droits sont OK voyons la suite"
	else
	echo "Les droits sont incorrects,changez les"
	exit 1
	fi	
	
	editor="/usr/bin/vim"
	if [ $EDITOR != $editor ];then
	echo "Mettez vim comme editeur par défaut"
	exit 1
	else
	echo "Vim est votre editeur par défaut !Excellent"
	fi

	echo "Creez maintenant 'compteur' un fichier contenant les nombre de 1 a 1000"
	shacpt=$(shasum compteur | awk '{print $1}' )
	if [ $shacpt == 234e7e9c9c8490946d3e8c2a01bff41e9acce269 ];then	
	echo "Bravo!Passons a la suite"
	else
	echo "Essaye encore... "
	exit 1
	fi
	
	echo "Vous devez maintenant mettre le nombre de ligne de /root/my_history dans un fichier 'nbligne'"
	echo "Pour ce faire creer un script qui ecrit le nombre de ligne du fichier dans le fichier nbligne"
	shnbl=$(shasum nbligne | awk '{print $1}')
	if [ $shnbl == 5e8e15f3df1dc69e865117a99a9e804f2d13e03f ];then
	echo "C'est bien bravo!"
	else
	echo "Dommage essaye encore"
	exit 1
	fi

	echo "Il faut maintenant mettre le nombre de ls du fichier /root/myhistory dans un fichier nbls"
	shnbls=$(shasum nbls | awk '{print $1}')	
	if [ $shnbls == 33ac14b607229120233d7251b8a303038444fe18 ];then
	echo "Mmmh il faut croire que c'est trop facile pour toi..."
	else
	echo "Essaye encore"
	exit 1
	fi
	
	echo "Un peu plus complexe , maintenant il va falloir ecrire le nombre de commande de l'historique lancée entre le 12 et le 15 Nov. dans un fichier 'nbcommande'"
	shnbcomm=$(shasum nbcommande | awk '{print $1}')
	if [ $shnbcomm == b57defb29ae2406a34d886d8694c76ca8998f757 ];then
	echo "Eh bien mon ami, chapeau bas!"
	else
	echo "Allez un peu de courage"
	exit 1
	fi
	
	echo "Allez un petit bonus alors, ecrit un script qui prend un nombre en parametre et l'eleve a sa propre puissance. Appelle ce script puissance.sh"
	echo " Exemple resultat : 2^2=4"
	test=$(./puissance.sh 5)
	if [ $test == '5^5=3125' ];then
	echo "Bravo tu as réussi! Tu as tout réussi c'etait une belle experience n'est-ce pas? Allez entraine toi encore a faire des scripts, ce sont tes meilleurs amis... A plus!"
	else
	echo "Courage tu peux le faire"
	exit 1		 
	fi

    else
	echo "Le fichier test n'existe pas , le creer pour continuer"
	exit 1
	fi  


else
    echo "Modifier le nom de ce script en monjeu.sh et exécuter le avec ./monjeu.sh"
    exit 1
fi
