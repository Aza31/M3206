#!/bin/bash
path=$(readlink -f $1)    #chemin où le fichier était initialement

rm -rf $1			#on supprime le répertoire précédent	
#on stocke dans la variable sortie le nom de l'archive de sauvegarde la plus récente:
#On fait une boucle pour recuperer les fichiers correspondant au pattern du script save.sh

fic=$(for fichier in `ls /home/backup | grep $(basename $1).tar.gz`

do	
 
	if [ $(tar -xOf /home/backup/$fichier $(basename $1)/path.txt) = $path ]

	then
		echo $fichier #on affiche tous les fichiers qui correspond à ce pattern
	fi
done | sort -r | head -1)

tar xvzf /home/backup/$fic -C $(dirname $path) #Ensuite on restore l'arborescence
echo $fic

