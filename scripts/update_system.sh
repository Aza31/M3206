#!/bin/bash
if [[ $EUID -eq 0 ]] 
#On verifie que l'utilisateur est root
then
   	echo "Upgrading system" 
	apt-get update -qq 2>/dev/null
#redirection message d'erreurs avec 2>
	apt-get upgrade -qq 2>/dev/null
	exit 0

else 	
	echo "Execute as root"
	
fi
